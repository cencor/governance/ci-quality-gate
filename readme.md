# Reglas de Cencor para CI

Este repositorio centraliza las reglas y politicas de las distintas herramientas que se utilizan actualmente en CENCOR. Los distintos proyectos de CENCOR haran referencia a las reglas aqui definidas.

Las reglas aqui definidas son publicadas al repositorio de artefactos de Nexus donde todos los proyectos pueden hacer referencia a las mismas:

* `Checkstyle` : http://ci.enlaceint.com/repository/maven-releases/com/cencor/governance/ci-quality-gate/CENCOR/ci-quality-gate-CENCOR-checkstyle.xml
* `Dependency Check` - Falsos positivos : http://ci.enlaceint.com/repository/maven-releases/com/cencor/governance/ci-quality-gate/CENCOR/ci-quality-gate-CENCOR-dependency-check-false-positives.xml
* `Dependency Check` - Falsos negativos : http://ci.enlaceint.com/repository/maven-releases/com/cencor/governance/ci-quality-gate/CENCOR/ci-quality-gate-CENCOR-dependency-check-false-negatives.xml


## Checkstyle

Incluye las reglas que se utilizaran para validar con `Checkstyle`. Las reglas de `Checkstyle` son las por defecto aunque agregando una validación de uso de espacios en vez de tabuladores.

Para ejemplificar su uso en `Gradle`:

```groovy
checkstyle {
    sourceSets = [project.sourceSets.main]
    config = resources.text.fromUri("http://ci.enlaceint.com/repository/maven-releases/com/cencor/governance/ci-quality-gate/CENCOR/ci-quality-gate-CENCOR-checkstyle.xml")
}
```

## Reglas de Dependency Check

Este repositorio concentra las reglas a aplicar en todos los proyectos de CENCOR con respecto a la herramienta Dependency-Check.

Actualmente se cuenta con la definicion de falsos positivos como de falsos negativos.

De este modo se concentra en un solo lugar aquellas dependencias que son vulnerables y que se asumira el riesgo por algun tiempo mientras se lleva a cabo alguna solucion o remediacion de la vulnerabilidad.

Para ejemplificar su uso en `Gradle`:

```groovy
dependencyCheck {
    failBuildOnCVSS=4
    suppressionFile="http://ci.enlaceint.com/repository/maven-releases/com/cencor/governance/ci-quality-gate/CENCOR/ci-quality-gate-CENCOR-dependency-check-false-positives.xml"
    hintsFile="http://ci.enlaceint.com/repository/maven-releases/com/cencor/governance/ci-quality-gate/CENCOR/ci-quality-gate-CENCOR-dependency-check-false-negatives.xml"
}
```

## ¿Qué hago para solicitar la modificación de una regla o adición de un falso positivo?

En caso de requerir modificar/agregar alguna regla de `Checkstyle` o alguna excepción/falso positivo de `Dependency-check`, envía tu propuesta como Merge request en donde se evaluará la misma. En caso de ser aprobada de integrará la propuesta y en breve se actualizará en el repositorio de artefactos de CENCOR. En caso de ser rechazada recibirás a cambio una contra propuesta con el fin de llegar a un acuerdo.